package main

import (
	"flag"
	"fmt"
	"strings"
	"os"
	"compress/gzip"
	"io"
	"bufio"
	"bikedata"
	"encoding/json"
	"log"
	"strconv"
)

func main() {
	// File from commandline
	var filename = flag.String("d", "<file path>", "please provide the path to the file as well")
	flag.Parse()
	// Read in a file from command line
	data, err := os.Open(*filename)
	if (err != nil) {
		fmt.Println("There was an error reading the file. bikeshare options:")
		flag.PrintDefaults()
		os.Exit(0);
	}
	// Init reader
	var reader io.Reader

	if (strings.HasSuffix(*filename, "gz")) {
		// Open the gzip stream
		//fmt.Println("Compressed encoding: ", *filename)
		// Create a reader for the gz file
		reader, _ = gzip.NewReader(data)
	} else {
		// Open the regular stream
		fmt.Println("Filename: ", *filename)
		reader = bufio.NewReader(data)
	}

	dec := json.NewDecoder(reader)
	for {
		var m bikedata.BikeData
		if err:= dec.Decode(&m)
		    err == io.EOF {
			    break
		    } else if err != nil {
			    log.Fatal(err)
		    }

		    for _, station := range m.StationBeanList {
			    //19
			    var values string
			    values += strconv.Itoa(station.Id) +","
			    values += "'"+strings.Replace(station.StationName,"'","''", -1)+ "',"
			    values += "'"+m.ExecutionTime + "',"
			    values += strconv.Itoa(station.AvailableDocks) + ","
			    values += strconv.Itoa(station.TotalDocks) + ","
			    if (station.Altitude != "") {
				    values += station.Altitude + ","
			    } else {
				    values += "NULL,"
			    }

			    if (station.City != "") {
				    values += station.City + ","
			    } else {
				    values += "NULL,"
			    }

			    if (station.LandMark != "") {
				    values += station.LandMark + ","
			    } else {
				    values += "NULL,"
			    }

			    if (station.LastCommunicationTime != nil) { 
				    values += *station.LastCommunicationTime + ","
			    } else {
				    values += "NULL,"
			    }

			    values += fmt.Sprintf("%g", station.Latitude) + ","

			    if (station.Location != "") {
				    values += "'"+station.Location + "',"
			    } else {
				    values += "NULL,"
			    }

			    values += fmt.Sprintf("%g", station.Longitude) + ","

			    if (station.PostalCode != "") {
				    values += "'"+station.PostalCode + "',"
			    } else {
				    values += "NULL,"
			    }

			    if (station.StAddress1 != "") {
				    values += "'"+station.StAddress1 + "',"
			    } else {
				    values += "NULL,"
			    }

			    if (station.StAddress2 != "") {
				    values += "'"+station.StAddress2 + "',"
			    } else {
				    values += "NULL,"
			    }
			    values += fmt.Sprintf("%d", station.StatusKey) + ","
			    values += "'"+station.StatusValue + "',"
			    values += fmt.Sprintf("%t", station.TestStation)

			    fmt.Printf("INSERT INTO station_info (stationid, stationname, created_at, availabledocks, totaldocks, altitude, city, landmark, lastCommunicationTime, latitude, location, longitude, postalcode, stAddress1, stAddress2, statusKey, statusValue, testStation) VALUES(%s);\n", values);
			    //fmt.Printf("INSERT INTO station_info (id, stationname, created_at, availabledocks, totaldocks, altitude, city, landmark, lastCommunicationTime, latitude, location, longitude, postalcode, stAddress1, stAddress2, statusKey, statusValue, testStation) VALUES(%d,'%s','%s',%d,%d,'%s','%s','%s','%s','%f','%s','%f','%s','%s','%s',%d,'%s',%t);\n", station.Id , station.StationName , m.ExecutionTime, station.AvailableDocks , station.TotalDocks, station.Altitude, station.City, station.LandMark, station.LastCommunicationTime, station.Latitude, station.Location, station.Longitude, station.PostalCode, station.StAddress1, station.StAddress2, station.StatusKey, station.StatusValue, station.TestStation )
			    //19
		    }
		    //fmt.Printf("%s\n", m.ExecutionTime)
	}
}





