package stationbean

type StationBean struct {
	Id			int
	StationName		string
	AvailableDocks		int
	TotalDocks		int
	Latitude		float32
	Longitude		float32
	StatusValue		string
	StatusKey		int
	AvailableBikes		int
	StAddress1		string
	StAddress2		string
	City			string
	PostalCode		string
	Location		string
	Altitude		string
	TestStation		bool
	LastCommunicationTime	*string
	LandMark		string
}
