package bikedata

import (
	sb "bikedata/stationbean"
)

type BikeData struct {
	ExecutionTime	    string		"ExecutionTime"
	StationBeanList	    []sb.StationBean	"stationBeanList"
}
