#!/bin/bash

while [ 1 ]; do
	TS=`date +%s`;
	echo "Current date [ ${TS} ]";
	CURRENT_DAY=`date +%Y%m%d`;
	if [ ! -f "${CURRENT_DAY}" ]; then
		mkdir -p data/${CURRENT_DAY};
	fi
	curl http://citibikenyc.com/stations/json | gzip > data/${CURRENT_DAY}/${TS}.gz
	sleep 60;
done;
