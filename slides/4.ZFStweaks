Goal: Setup some basic ZFS options
User: root

0) Blocksize[0]
    - Postgres expects 8K blocksize
    - ZFS lets you tune the recordsize.
	- zfs set recordsize=8K tank/pg

1) Compression
    - What is it?
	- Compression is a great way to save IO at the expense of computation.
	- Turn on Compression for a ZFS directory:
	    - zfs set compression=gzip tank/pg/bikeshare
	- Lots of options 'man zfs' Search for 'compression'.
	    - lzjb
	    - gzip
	    - gzip-N
	    - zle
	- FreeBSD 9.2 has lz4 [1]
	    - "Rockstar mode" [2]
		- lz4 vs. lzjb
		    - 50% faster compression on compressible data
		    - 80% faster on decompression
		    - 3x faster on compression of incompressible data
		    - modern CPUs through put 500 MB/s on compression and 1.5GB/s on decompression (per core)

2) Block deduplication
    - What is it?
	- ZFS Deduplication Oracle Blogs [1]
	- Works on files, record or block level
	- General advise,"1GB of ram for every 1TB in the pool"
	    - Pros
		- Saves on writes, in theory space.
	    - Cons
		- Performance hit when the mappings of existing blocks don't fit in memory
	- Use it?
	    - This is really dependent on your use case.
	    - When this might help
		- Good database size to ram ratio
		- Poorly designed schema with lots of denormalized data
		- Repetive writes

2) Snapshots
    - Why?
	- Replication to new hosts
	- Backups
	- Developer environments
    - Watch should I watch out for?
	- Race condition between data and xlogs
	    - xlog rotate before you finish the data snapshot.
    - How?
	- Use an existing suite: https://github.com/omniti-labs/pgtreats/blob/master/tools/zbackup2.sh

[0] http://www.solarisinternals.com/wiki/index.php/ZFS_for_Databases#PostgreSQL_Considerations
[1] https://blogs.oracle.com/bonwick/entry/zfs_dedup
[2] http://wiki.illumos.org/display/illumos/LZ4+Compression
