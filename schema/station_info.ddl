DROP TABLE IF EXISTS station_info;

CREATE TABLE station_info (
    id			BIGSERIAL primary key
    , StationId		int
    , StationName	text
    , created_at	timestamp without time zone
    , availableDocks	int
    , totalDocks	int
    , altitude		bigint
    , city		bigint
    , landMark		bigint
    , lastCommunicationTime timestamp without time zone
    , latitude		real
    , location		text
    , longitude		real
    , postalCode	text
    , stAddress1	text
    , stAddress2	text
    , statusKey		int
    , statusValue	text
    , testStation	boolean
)
;
